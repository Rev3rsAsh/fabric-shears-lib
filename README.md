![Game version](https://img.shields.io/badge/Game%20version-1.19.4-yellowgreen?style=for-the-badge)
![Mod loader](https://img.shields.io/badge/Mod%20loader-Fabric-important?style=for-the-badge)
![License](https://img.shields.io/badge/License-CPL-informational?style=for-the-badge)

A library mod for Fabric, which makes modded ShearsItems behave just like the Shears item in both mob and block interactions.

### How does it work?

Normally, the code of shearing interactions (shearing a Sheep, a Mooshroom, etc.) and block loot_tables check only whether a player has used specifically a minecraft:shears item. This modification replaces each of these checks to accept any item located in a fabric:items/shears tag.

### Building

In order to get the .jar file, you have to import this project into IDE of your choice and then build it.

To import it, simply clone this repository into your IDE (Project from Version Control in Intellij IDEA).

To build it, execute the "gradlew build" command (without "") in the project's folder. The mod's .jar file should then be generated in the ./build/libs folder.

### Usage

This mod doesn't have to be imported as a project dependency.

Simply create a fabric:items/shears tag in your modification, add namespaces of shears in the form of <mod_id>:<item_name> with an option on top to not replace the tag ("replace": false).
Then add this mod to your game instance.