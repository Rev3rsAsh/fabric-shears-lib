package com.Rev3rsAsh.fabric_shears_lib.mixins;

import net.minecraft.block.BeehiveBlock;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.item.ShearsItem;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Redirect;

@Mixin(BeehiveBlock.class)
public class BeehiveBlockMixin {
    @Redirect(at=@At(value = "INVOKE", target = "Lnet/minecraft/item/ItemStack;isOf(Lnet/minecraft/item/Item;)Z", ordinal = 0), method= "onUse")
    private boolean allowModdedShears(ItemStack itemStack, Item item) {
        return itemStack.getItem() instanceof ShearsItem;
    }
}
