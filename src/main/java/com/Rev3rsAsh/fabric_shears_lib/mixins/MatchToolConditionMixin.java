package com.Rev3rsAsh.fabric_shears_lib.mixins;

import com.Rev3rsAsh.fabric_shears_lib.ModCommon;
import net.minecraft.item.ItemStack;
import net.minecraft.item.Items;
import net.minecraft.loot.condition.MatchToolLootCondition;
import net.minecraft.loot.context.LootContext;
import net.minecraft.loot.context.LootContextParameters;
import net.minecraft.predicate.item.ItemPredicate;
import org.spongepowered.asm.mixin.Final;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Shadow;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;

@Mixin(MatchToolLootCondition.class)
public abstract class MatchToolConditionMixin {
    @Shadow @Final
    ItemPredicate predicate;

    @Inject(at=@At(value = "TAIL"), method= "test(Lnet/minecraft/loot/context/LootContext;)Z", cancellable = true)
    private void allowModdedShears(LootContext lootContext, CallbackInfoReturnable<Boolean> cir) {
        ItemStack itemStack = lootContext.get(LootContextParameters.TOOL);
        if (itemStack != null && itemStack.isIn(ModCommon.SHEARS))
            cir.setReturnValue(this.predicate.test(Items.SHEARS.getDefaultStack()));
    }
}