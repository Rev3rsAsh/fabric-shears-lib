package com.Rev3rsAsh.fabric_shears_lib.mixins;

import net.minecraft.entity.passive.SheepEntity;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.item.ShearsItem;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Redirect;

@Mixin(SheepEntity.class)
public class SheepEntityMixin {
    @Redirect(at=@At(value = "INVOKE", target = "Lnet/minecraft/item/ItemStack;isOf(Lnet/minecraft/item/Item;)Z", ordinal = 0), method= "interactMob")
    private boolean allowModdedShears(ItemStack itemStack, Item item) {
        return itemStack.getItem() instanceof ShearsItem;
    }
}
