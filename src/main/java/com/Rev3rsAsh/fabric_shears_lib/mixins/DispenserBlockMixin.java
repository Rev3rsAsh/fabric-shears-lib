package com.Rev3rsAsh.fabric_shears_lib.mixins;

import com.Rev3rsAsh.fabric_shears_lib.ModCommon;
import net.minecraft.block.DispenserBlock;
import net.minecraft.block.dispenser.DispenserBehavior;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.item.Items;
import org.spongepowered.asm.mixin.Final;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Shadow;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;

import java.util.Map;

@Mixin(DispenserBlock.class)
public abstract class DispenserBlockMixin {
    @Shadow @Final
    private static Map<Item, DispenserBehavior> BEHAVIORS;

    @Inject(at=@At(value = "HEAD"), method= "getBehaviorForItem", cancellable = true)
    private void getBehaviorForModdedShears(ItemStack itemStack, CallbackInfoReturnable<DispenserBehavior> cir) {
        if (itemStack.isIn(ModCommon.SHEARS))
            cir.setReturnValue(BEHAVIORS.get(Items.SHEARS));
    }
}
