package com.Rev3rsAsh.fabric_shears_lib;

import net.fabricmc.api.ModInitializer;
import net.minecraft.item.Item;
import net.minecraft.registry.RegistryKeys;
import net.minecraft.registry.tag.TagKey;
import net.minecraft.util.Identifier;

public class ModCommon implements ModInitializer {
    public static final TagKey<Item> SHEARS = TagKey.of(RegistryKeys.ITEM, new Identifier("fabric", "shears"));

    @Override
    public void onInitialize() {}
}